
# coding: utf-8

# In[40]:

'''
MARKOV_CHAIN.PY

This program uses a Markov chain
with five steps to compare this
probability distribution after 50
time steps to a stationary 
distribution to see if they are 
equivalent to a tolerance of 1e-5.
The entries of our transition matrix
will be randomized and normalized.

INITIAL PARAMETERS: 
  
  No user inputs required. 
  
  states = 5: The markov chain will
  take on five states which will
  make our transition matrix 5 by 5
  
  steps = 50: The markov chain will 
  be computed for 50 time steps to 
  see how this value differs from 
  the stationary distribution

OUTPUTS: 

  A printed statement verifying whether
  or not the Markov chain and 
  stationary distribution is equal to 
  the tolerance of 1e-5.
  
Other python files required: None
Subfunctions: None
            
AUTHOR: Roberto Bertolini
ID: 111635119
Email: roberto.bertolini@stonybrook.edu
Date: October 29, 2017

'''

import numpy as np

states = 5

# Creates the randomized probability
# matrix on the five states. This
# is normalized by dividing by the sum.

prob_dist = np.random.rand(states)
prob_dist = prob_dist / prob_dist.sum()

# Creates the randomized transition matrix and 
# then normalizes the rows by dividing by their 
# row sum.

transition_mat = np.random.rand(states, states)
transition_mat = transition_mat / transition_mat.sum(axis=1)[:,np.newaxis]

# Calculates the 50th step from the random
# normalized probability distribution

# .dot command performs matrix multiplication

steps = 50
for i in range(steps):
    prob_dist = transition_mat.T.dot(prob_dist)

prob_dist_50 = prob_dist

'''

# Calculates the eigenvalues and eigenvectors
# of the tranpose of the transition matrix 
# using the linalg.eig command. Note that 
# this command normalilzes the eigenvectors
# for each column so I do not understand why the
# directions remind the reader to normalize the 
# eigenvectors. 

# ew - eigenvalue
# ev - eigenvector

'''
ew, ev = np.linalg.eig(transition_mat.T)

'''

Finds the eigenvalue which is closest to 
one by taking the difference between the 
eigenvalues and 1.0, and uses the argmin 
command to return the index of the array 
that contains this eigenvalue.

'''

temp_stationary = np.abs(ew - 1.0).argmin()

# Takes the corresponding eigenvectors and 
# removes the imaginary part which is zero

p_stationary = ev[:,temp_stationary]

# Normalizes the p_stationary vector - I think
# this is what the directions mean by normalizing
# the eigenvectors.

p_stationary_norm = p_stationary / p_stationary.sum()

# Defines the tolerance and prints whether or not the 
# differnece between the stochastic and stationary
# distribution is less than 1e-5.

tolerance = 1e-5
diff = prob_dist_50 - p_stationary_norm
print("Difference between stationary and p_50 components: {}".format(diff))  
if np.linalg.norm(prob_dist_50 - p_stationary_norm) < tolerance:
    print ("{} tolerance holds".format(tolerance))
if np.linalg.norm(prob_dist_50 - p_stationary_norm) >= tolerance:
    print ("{} tolerance DOES NOT hold".format(tolerance))

